import pygame, sys,os
from pygame.locals import *
from math import *
from vector3 import *
from matrix3 import *
import math

class engine():
    def __init__(self,theta1,theta2,theta3):
	###theta1 = self.theta1 = sqrt(2)/2 
	###theta2 = self.theta2 = sqrt(2)/2 
	###theta3 = self.theta3 = sqrt(2)/2 
	self.mx = Matrix3(1,0,0,
			0,math.cos(theta1),-math.sin(theta1),
			0,math.sin(theta1),math.cos(theta1))
	self.my = Matrix3(math.cos(theta2)/1.0,0, math.sin(theta2)/1.0,
			0,1,0,
			-math.sin(theta2),0, math.cos(theta2))
	self.mz = Matrix3(math.cos(theta3)/1.0, -math.sin(theta3)/1.0, 0,
			math.sin(theta3)/1.0, math.cos(theta3)/1.0, 0,
			0,0,1)


    def rotate(self, t1,t2,t3):
	self.mx = Matrix3(1,0,0,
			0,math.cos(t1),-math.sin(t1),
			0,math.sin(t1),math.cos(t1))
	self.my = Matrix3(math.cos(t2)/1.0,0, math.sin(t2)/1.0,
			0,1,0,
			-math.sin(t2),0, math.cos(t2))
	self.mz = Matrix3(math.cos(t3)/1.0, -math.sin(t3)/1.0,0.0,
			math.sin(t3)/1.0, math.cos(t3)/1.0, 0.0,
			0.0,0.0,1.0)

#    def drawLine(self, x1,y1,x2,y2):
#	xlen = x2-x1
#	ylen = y2-y1
#	xmod = xlen / (ylen + 1)#Float division with 0
#	ymod = ylen / (xlen + 1)
#	yy = y1
#	if x1<x2:
#	    xx = x1
#	    while xx < x2 or yy < y2:
#	        self.screen.blit(self.surf, (xx, yy))
#	        xx += xmod
#	        yy += ymod
#	else:
#	    xx = x2
#	    while xx < x1 or yy < y2:
#	        self.screen.blit(self.surf, (xx, yy))
#	        xx += xmod
#	        yy += ymod
	    

    def display(self, v1, v2):
	m = self.mx.multiplyByMatrix(self.my)
	m = m.multiplyByMatrix(self.mz)
	retv1 = m.multiply(v1)
	retv2 = m.multiply(v2)
	pygame.draw.line(self.screen, (255,255,255),
			(retv1.array[0],retv1.array[1]),
			(retv2.array[0],retv2.array[1]))
	return retv1
