
# Copyright (C) Johan Ceuppens 2011
# Copyright (C) Johan Ceuppens 2010

# Copyright (C) Johan Ceuppens 2009 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from myconid import *
from food import *
from capricornguardian3 import *
from bombman import *

class Entityfactory(object):
    
    def makewith(self, entityType,x,y,w,h, boxes, entities, player):
        classByType = {
            Entity.ENTITY_CAPRICORNGUARDIAN3 : Capricornguardian3,
            Entity.ENTITY_BOMBMAN1 : Bombman,
        }
        return classByType[entityType](x,y,w,h, boxes, entities,player)

    def make(self, entityType,x,y,w,h):
        classByType = {
            Entity.ENTITY_MYCONID : Myconid,
            Entity.ENTITY_FOOD : Food,
        }
        return classByType[entityType](x,y,w,h)

    def add(self,room):
        room.entityfactory = self

