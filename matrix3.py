import pygame, sys,os
from pygame.locals import *

from vector3 import *

class Matrix3():
    def __init__(self, xx1,yy1,zz1,xx2,yy2,zz2,xx3,yy3,zz3):
	self.array = [xx1,yy1,zz1,xx2,yy2,zz2,xx3,yy3,zz3]


    def transpose(self):
	m = Matrix3(0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0)
	array = [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]
	array[0] = self.array[0]
	array[1] = self.array[3]
	array[2] = self.array[6]
	array[3] = self.array[1]
	array[4] = self.array[4]
	array[5] = self.array[7]
	array[6] = self.array[2]
	array[7] = self.array[5]
	array[8] = self.array[8]
	m.array = array
        return m	

    def multiply(self, v1):
	retv = Vector3(0.0,0.0,0.0)
	for j in [0,1,2]:
	    for i in [0,1,2]:
	        retv.array[i] += self.array[j*3+i] * v1.array[i]
	return retv

    def multiplyByMatrix(self, m1):#FIXME sum
	retm = Matrix3(0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0)
	m = m1.transpose()
	for k in [0,1,2,3,4,5,6,7,8]:
	    for j in [0,1,2]:
	        retm.array[k] += self.array[j] * m.array[j]
	return retm
    
