
# Copyright (C) Johan Ceuppens 2011
# Copyright (C) Johan Ceuppens 2010

# Copyright (C) Johan Ceuppens 2009 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import pygame
from pygame.locals import *
from bombman import *
from player import *
from level import *
from screen import *
#from utility import *
from mouse import *
from textenglish import *
#from textdutch import *
from inventory import *
from inventoryscrollinvisibility import *

class Game:
    "Main function"
    def __init__(self):
        pygame.init()
        pygame.font.init()
        self.font = pygame.font.SysFont("Vera", 32)
	self.counter = 0
        self.language = "english"
        
#        if self.language == "english":
        self.text = Textenglish()
#        elif self.language == "dutch":
#            self.text = Textdutch()

        self.screen = Screen()
        self.mouse = Mouse()
	self.inventoryitemtext = None 
        self.inventoryitem = None

	self.swordplay = 0
	self.movedflag = 0

        self.player = Player()
        self.level = Level(self.screen,self.player,self.font)

       	blankimage = pygame.image.load('./pics/blank.bmp').convert()


	self.player.x = 320
	self.player.y = 250
##	while self.player.y > 250:
##            self.level.roommanager.room.collisionexits(self.player,self.level.roommanager)
##            self.level.roommanager.room.update(self.player,self.level, self.swordplay)                
##            self.level.roommanager.room.draw(self.screen)
##	    self.player.moveupanddirect1()
##	    self.screen.scr.blit(self.player.draw(), (self.player.x,self.player.y))
##            pygame.display.update()
##            self.screen.scr.blit(blankimage, (0,0))

        while 1:#self.level.gameover == 0:

            for event in pygame.event.get():
                if event.type == QUIT:
                    return
                elif event.type == KEYDOWN:
			for i in range(0,self.level.roommanager.room.trees.length):
                    		tempoct = self.level.roommanager.room.collisionboxes(self.player) or self.level.roommanager.room.trees.getwithindex(i).collision(self.player,self)
		    		self.movedflag = 1
                    		if event.key == K_SPACE:
		       			if tempoct == 1:## or tempoct == 1:
						self.player.jump(self,1)
		       			else:
						self.player.jump(self,0)
					break
                    		if event.key == K_UP:
		           		if tempoct == 1: 
                           			self.player.bounceback(self.level)
      	                        		print "Collision player with box"
			   		elif tempoct == 2:
                        			self.player.moveupanddirect(self.level)
						self.player.depth += 1
                     	   		else:
                        			self.player.moveupanddirect(self.level)
					break
                    		elif event.key == K_DOWN:
                    	   		tempoct2 = self.level.roommanager.room.collisionboxes(self.player) or self.level.roommanager.room.octtree.collision(self.player,self)
                       	   		if tempoct2:
                       	        		self.player.movedownanddirect(self.level)
						self.player.depth += 1
			   		elif not tempoct2:
						self.player.jump(self,0)
                           		elif tempoct == 1:
			   			self.player.bounceback(self.level)
               	                		print "Collision player with box"
			   		elif tempoct == 2:
                        			self.player.movedownanddirect(self.level)
					break
                    		elif event.key == K_LEFT:
                           		if tempoct == 1:
						self.player.bounceback(self.level)
                   	       			print "Collision player with box"
                           		else:
                        			self.player.moveleftanddirect(self.level)
					break
                    		elif event.key == K_RIGHT:
                           		if tempoct == 1:
						self.player.bounceback(self.level)
                                		print "Collision player with box"
                           		else:
                        			self.player.moverightanddirect(self.level)

					break
                    		elif event.key == K_u:
#                        self.level.gameover = 1
                      #FIXME  pygame.event.flush()
                       			flag = 0
                        		inventory = Inventory()
		
					if Scrollinvisibility(0,0,0,0,"1","1").readkeys(None):
                            			inventory.additem(Inventoryscrollinvisibility())
	
					while flag == 0:#NOTE1
                            			for event in pygame.event.get():
                               		 		if event.type == QUIT:
                               		     			return

                             		   		elif event.type == KEYDOWN:
                                    				if event.key == K_LEFT:
                                        				inventory.moveleft()
                                    				elif event.key == K_RIGHT:
                                        				inventory.moveright()
                                    				elif event.key == K_RETURN:
                                        				self.inventoryitem = inventory.getitem(self.inventoryitem)

                                        		self.level.gameover = 0
                                        		flag = 1


                                	inventory.draw(self.screen)
                                	pygame.display.update()
					break
 

	    ### FIX trees
          ###  temp1 = self.level.roommanager.room.octtree.collision(self.player,self)
###	    if temp1 == 2 or temp1 == 1:
		1
	    ###else:
	###	self.player.jump(self,0)###FIX self.trees
			
		1###if self.player.depth < 0:
            self.level.roommanager.room.collisionexits(self.player,self.level.roommanager)
            self.level.roommanager.room.update(self.player,self.level, self.swordplay)                
            self.level.roommanager.room.draw(self.screen)
            ### fall         
            ###if not self.level.roommanager.room.octtree.collisiony(self.player):
	###	self.player.y -= 1	

###	    bombman = Bombman(400,100)
###            if bombman.draw(self.level.roommanager.room):
###		1

            if self.movedflag:
		self.movedflag = 0 
		self.screen.scr.blit(self.player.draw(), (self.player.x,self.player.y))
            else:
		self.screen.scr.blit(self.player.draw0(), (self.player.x,self.player.y))

            self.mouse.update()
            self.mouse.draw(self.screen,self.inventoryitem)
            
            # FIXME1 maproom 
            if self.inventoryitem:
                self.inventoryitemtext = self.level.roommanager.room.domouseinventory(self)
		if self.inventoryitemtext:
		    self.counter = 100
                    self.inventoryitem = None
            else:
                self.level.roommanager.room.domouse(self)#self.screen, self.mouse,font1,self.text,self.player)

#self.level.roommanager.room.domouse(self)#self.screen, self.mouse,font1,self.text,self.player)
	    if self.inventoryitemtext and self.counter > 0:
		self.counter -= 1
		self.screen.scr.blit(self.font.render(self.inventoryitemtext, 16, (255,255,255)),(10,10))
            pygame.display.update()
            self.screen.scr.blit(blankimage, (0,0))
       
#	while 1: 
#       	   image1 = pygame.image.load('gameover1.bmp').convert()
#           self.screen.scr.blit(image1, (0,0))
#           pygame.display.update()

        
        
        if not Utilities().readandfindstring(string):
                    
            f = open("./workfile",'r')
            for line in f:
                s += line
                
                f = open("./workfile",'w')
        
                f.write(s)
                f.write(string)
                f.write("\n")
                

    def domouseonplayer(self,room):
        room.collisionplayer(self.mouse,self.player)

    def domouseonplayerrelative(self,room):
        room.collisionplayerrelative(self.mouse,self.player,room.relativex,room.relativey)


    def leftbuttonpressed(self,o):
        self.previoustext = o.leftbuttonpressed(self)#self,game.text,self.press(0))
        return self.previoustext
                        
    def collisionlistonmouse(self,list):
        for i in range(0,list.length):
            o = None
            #o = list.getlistobject(o)
	    o = list.getwithindex(i)

            if o and self.mouse.x >= o.x and self.mouse.x <= o.x + o.w and self.mouse.y >= o.y and self.mouse.y <= o.y + o.h:   	
                return o
        return None

    # Relative processors
    def processorrelativex(self,o,relx):
        return o.x - relx

    def processorrelativey(self,o,rely):
        return o.y - rely

    def collisionlistrelativeonmouse(self,list,relx,rely):
        for i in range(0,list.length):
            o = None
            o = list.getlistobject(o)

            if o:
            
                x = o.x
                y = o.y
                mx = self.processorrelativex(self.mouse,relx)
                my = self.processorrelativey(self.mouse,rely)

                if o and mx >= x and mx <= x + o.w and my >= y and my <= y + o.h:   	
                    return o
        return None



    def update(self):
        "move the fist based on the mouse position"
        
#        pos = pygame.mouse.get_pos()
#        self.rect.midtop = pos
#        if self.punching:
#            self.rect.move_ip(5, 10)
    

if __name__ == "__main__":
   foo = Game()
