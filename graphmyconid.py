
# Copyright (C) Johan Ceuppens 2011
# Copyright (C) Johan Ceuppens 2010

# Copyright (C) Johan Ceuppens 2009 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from graph import *

class Graphmyconid(object):
    def __init__(self):
        self.graph = None
        self.currentnode = None

    def get(self,text):

        if not self.graph or not self.currentnode:#runtime load of textgraph
            self.graph = Graph(text.getmyconid1())
            self.currentnode = self.graph
            self.graph.next.next = Graphnode(text.getmyconid2())

        if self.currentnode.next:# dummy start of queue in graph.py
            self.currentnode = self.currentnode.next
            return self.currentnode.node
        else:
            self.currentnode = self.graph
        return None
            
