
# Copyright (C) Johan Ceuppens 2011
# Copyright (C) Johan Ceuppens 2010

# Copyright (C) Johan Ceuppens 2009 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import pygame
from pygame.locals import *

#from key import *
#from lockeddoornorth import *
#from lockeddoorsouth import *
#from lockeddooreast import *
#from lockeddoorwest import *

from world import *
#from mapworld import *

class Roommanager:
    "Room manager"
    def __init__(self, screen, player,font):
	self.screen = screen
        self.room = None
	self.player = player
	self.font = font
        self.load(Room2world(self))
###        self.load(Room1world(self))
###        self.load(DungeonundertreeCastle(-100,-100,self))
#        self.load(Worldmapinsidetemple5(-100,-300,None))
#        self.load(Dungeonwaterlower2(None))
#        self.load(Dungeonwater5(-200,-100,None))
#        self.load(Roomdungeonwatercherubs(None))
#        self.load(Roombirdforest(-20,-200,None))
#        self.load(Worldmaproom3(-1000,-100, None))
        # other entries in the game :

#        self.load(Room5dungeon1())

#        self.load(Room9dungeon2())
#        self.load(Worldmaproom1(0,-520))
     
    def load(self,room):
        self.room = room.loadroom()

    def getroom(self):
        return self.room
    
