
# Copyright (C) Johan Ceuppens 2011
# Copyright (C) Johan Ceuppens 2010

# Copyright (C) Johan Ceuppens 2009,2010 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import pygame
from pygame.locals import *

from entity import *

from random import *
import os

from utility import *

class Food(Entity):
    def __init__(self,x,y,w,h):
        Entity.__init__(self,x,y,w,h)
	if randint(0,1) == 0:
            self.addimage('./pics/food3.bmp',255,255,255)
        else:
            self.addimage('./pics/food3.bmp',255,255,255)
#        self.addimage('food3.bmp',0,0,0)

    def update(self,level):
        1

    def deathupdate(self):
        1
        
    def remove(self,list):
	print ("You eat some food.\n")
        list.remove(self)
	return None
    
    def dofood(self,player):
	player.increasehp()
 
    def getname(self,text):
        return text.getfoodname()
