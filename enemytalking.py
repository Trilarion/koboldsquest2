
# Copyright (C) Johan Ceuppens 2011
# Copyright (C) Johan Ceuppens 2010

# Copyright (C) Johan Ceuppens 2009 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from enemy import *

class Enemytalking(Enemy):
    def __init__(self,x,y,w,h,talksystem):
        Enemy.__init__(self,x,y,w,h)
        self.talksystem = talksystem
        self.talkcounter = 0
        self.currenttext = ""

    def leftbuttonpressed(self,game):#mouse,text,press0):
        if game.mouse.press(0) == 1:
            self.talkcounter += 1
            if self.talkcounter == 1:
                self.currenttext = self.talksystem.gettalk(game.text)#foo
            return self.currenttext
        else:
            self.talkcounter = 0 	
            return self.currenttext

    def leftbuttonpressedwithinventoryitem(self,game):
        if game.mouse.press(0) == 1:
            self.talkcounter += 1
		### If there is an inventory item has been chosen, return string
            if game.inventoryitem:
                return game.inventoryitem.use(self,game)
            return 1
        else:
            self.talkcounter = 0 	
            return None#self.currenttext

    def remove(self,list):
        if self.hitpoints <= 0:
            list.remove(self)    
