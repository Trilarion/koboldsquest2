import pygame, sys,os
from pygame.locals import *



class Vector3():
    def __init__(self, xx,yy,zz):
	self.array = [xx,yy,zz]
	
    def multiply(self, v1):
	retv = Vector3(1.0,1.0,1.0) 
	for i in [0,1,2]:
	    retv.array[i] = self.array[i] * v1[i]
	return retv

    def dotp(self, v1):
	ret = 0.0
	for i in [0,1,2]:
	    ret += self.array[i] * v1[i]
	return ret
