
# Copyright (C) Johan Ceuppens 2011
# Copyright (C) Johan Ceuppens 2010

# Copyright (C) Johan Ceuppens 2009 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from list import *

class OcttreeLeaf:
    "Octotree Leaf"
    def __init__(self, x,y,z,w,h,d):
	self.x = x 
	self.y = y 
	self.z = z 
	self.w = w 
	self.h = h 
	self.d = d 
	self.collide = 0 

    def isleaf(self):
	return 1

class OcttreeNode(OcttreeLeaf):
    "Octotree Node"
    def __init__(self,x,y,z,w,h,d):
        OcttreeLeaf.__init__(self,x,y,z,w,h,d)
	self.nodes = List() 

    def isleaf(self):
	return 0

    def add(self,n):
        self.nodes.addobject(n)


class Octtree:
    "Octtree"
    def __init__(self,x,y,z,w,h,d,depth):
	self.tree = OcttreeNode(x,y,z,w,h,d)
	self.depth = depth
	
    def add(self,n):
        self.tree.addobject(n)

    def generatetree(self,depth,width):
	self.generatetreerec(depth,self.tree,width,self.x,self.y,self.z,self.w,self.h,self.d)

    def generatetreeroom1(self,depth,width):
	self.generatetreerecroom1(depth,self.tree,self.tree.x,self.tree.y,self.tree.z,self.tree.w,self.tree.h,self.tree.d,300)

    def generatetreerec(self,depth,node,x,y,z,w,h,d):
	if depth <= 0:
		node.add(OcttreeLeaf(x,y,z,w,h,d))
		return 
###	for i in range(0,9):
	node.add(OcttreeNode(x,y,z,w/3,h/3,d/3))
	node.add(OcttreeNode(4*x/3,y,z,w/3,h/3,d/3))
	node.add(OcttreeNode(5*x/3,y,z,w/3,h/3,d/3))
	node.add(OcttreeNode(x,4*y/3,z,w/3,h/3,d/3))
	node.add(OcttreeNode(x,5*y/3,z,w/3,h/3,d/3))
	node.add(OcttreeNode(x,y,z,w/3,h/3,d/3))
	node.add(OcttreeNode(x,y,4*z/3,w/3,h/3,d/3))
	node.add(OcttreeNode(x,y,5*z/3,w/3,h/3,d/3))
##		print "-> %d" % i
	self.generatetreerec(depth-1,node.nodes.getwithindex(0),x,y,z,w/3,h/3,d/3)
	self.generatetreerec(depth-1,node.nodes.getwithindex(1),4*x/3,y,z,w/3,h/3,d/3)
	self.generatetreerec(depth-1,node.nodes.getwithindex(2),5*x/3,y,z,w/3,h/3,d/3)
	self.generatetreerec(depth-1,node.nodes.getwithindex(3),x,4*y/3,z,w/3,h/3,d/3)
	self.generatetreerec(depth-1,node.nodes.getwithindex(4),x,5*y/3,z,w/3,h/3,d/3)
	self.generatetreerec(depth-1,node.nodes.getwithindex(5),x,y,z,w/3,h/3,d/3)
	self.generatetreerec(depth-1,node.nodes.getwithindex(6),x,y,4*z/3,w/3,h/3,d/3)
	self.generatetreerec(depth-1,node.nodes.getwithindex(7),x,y,5*z/3,w/3,h/3,d/3)


    def generatetreerecroom1(self,depth,node,x,y,z,w,h,d,border):
	if depth <= 0:
		node.add(OcttreeLeaf(x,y,z,w,h,d))
		return 
	node.add(OcttreeNode(x,y,z,w/3,h/3,d/3))
	node.add(OcttreeNode(4*x/3,y,z,w/3,h/3,d/3))
	node.add(OcttreeNode(5*x/3,y,z,w/3,h/3,d/3))
	node.add(OcttreeNode(x,4*y/3,z,w/3,h/3,d/3))
	node.add(OcttreeNode(x,5*y/3,z,w/3,h/3,d/3))
	node.add(OcttreeNode(x,y,z,w/3,h/3,d/3))
	node.add(OcttreeNode(x,y,4*z/3,w/3,h/3,d/3))
	node.add(OcttreeNode(x,y,5*z/3,w/3,h/3,d/3))
	if w < border:
		node.collide = 1
	##print "-> %d" % i
	self.generatetreerec(depth-1,node.nodes.getwithindex(0),x,y,z,w/3,h/3,d/3)
	self.generatetreerec(depth-1,node.nodes.getwithindex(1),4*x/3,y,z,w/3,h/3,d/3)
	self.generatetreerec(depth-1,node.nodes.getwithindex(2),5*x/3,y,z,w/3,h/3,d/3)
	self.generatetreerec(depth-1,node.nodes.getwithindex(3),x,4*y/3,z,w/3,h/3,d/3)
	self.generatetreerec(depth-1,node.nodes.getwithindex(4),x,5*y/3,z,w/3,h/3,d/3)
	self.generatetreerec(depth-1,node.nodes.getwithindex(5),x,y,z,w/3,h/3,d/3)
	self.generatetreerec(depth-1,node.nodes.getwithindex(6),x,y,4*z/3,w/3,h/3,d/3)
	self.generatetreerec(depth-1,node.nodes.getwithindex(7),x,y,5*z/3,w/3,h/3,d/3)


    def collision(self,x,y,z):
	self.collisionrec(self.depth,self.tree,x,y,z)

    def collisionrec(self,depth,node,x,y,z):
	if depth <= 1:
		if node.collide == 1:
			return 1
		else:
			return 0
	for i in range(0,8):
		print "-> %d" % i
		if x < node.x and y < node.y and z < node.z:
			if node.collide == 1:
				return 1
		elif node.isleaf():
			return 0 
		else:
			self.collisionrec(depth-1,node.nodes.getwithindex(i),x,y,z)	
