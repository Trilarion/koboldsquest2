
# Copyright (C) Johan Ceuppens 2009 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import pygame

#from Numeric import *
#import pygame.surfarray as surfarray

from pygame.locals import *
from maproom import *
from room import *
from roomroomman import *
from box import *

from scrollinvisibility import *

class DungeonundertreeCastle(Room):
    "Dungeon under castle of assassin queen's dungeon - north of triple crossroads"

    def __init__(self,x,y, roomman):
        Room.__init__(self)
	self.roomman = roomman
	self.player = self.roomman.player

    def loadroom(self):
        self.initroom()
        self.loadroom1()
        return self

    def loadroom1(self):

	factory = Entityfactory()

	# OUTER WALLS
	self.addbox(0, 0, 300, 100)
	self.addbox(400, 0, 100, 100)
	self.addbox(0, 0, 100, 480)
	self.addbox(540, 0, 100, 480)
	self.addbox(0, 380, 640, 100)

	# STATUES 
	### self.addbox(475, 75, 70, 70)
	
	# ITEMS 
        # ENTITIES
	# ENEMIES
        ###self.addentity(factory.make(Entity.ENTITY_CAPRICORNGUARDIAN3,300,110,50,50,self.boxes,self.roomman.player, self.entities))
        self.addenemy(factory.makewith(Entity.ENTITY_CAPRICORNGUARDIAN3,300,110,50,50,self.boxes,self.entities,self.player))
        self.addenemy(factory.makewith(Entity.ENTITY_CAPRICORNGUARDIAN3,350,110,50,50,self.boxes,self.entities,self.player))
	# EXITS
	# NORTH EXIT
        ###self.addexit(300,0,100,100,4008,300,380,DungeonundertreeCastle8(0,0, self.roomman))
	# SOUTH EXIT
        ###self.addexit(300,380,100,100,4002,300,125,DungeonundertreeCastle4(0,0, self.roomman))
        self.background = pygame.image.load('./pics/bg_dungeonundertreecastle7.bmp').convert()

class Room1world(Room):
    ""
    def __init__(self, roomman):
        Room.__init__(self)
	self.roomman = roomman

    def loadroom(self):
        self.initroom()
        self.loadworldroom1()
        return self


    def loadworldroom1(self):
        
        factory = Entityfactory()
        self.addfactory(factory)

#	item = Scrollinvisibility(300,220,50,50,"1","1")
#        self.addentity(item)
#        item.readkeys(self.entities)

#        self.addentity(factory.make(Entity.ENTITY_TREE2, 0,0,130,130))

#        self.addentity(factory.make(Entity.ENTITY_FOOD,120,350,40,40))

#        self.addenemy(factory.make(Entity.ENTITY_SKELETON2,400,390,50,50))     #         self.addenemy(factory.make(Entity.ENTITY_SKELETON2,450,390,50,50))
#        self.addenemy(factory.make(Entity.ENTITY_MYCONID,400,390,50,50))     
#         self.addenemy(factory.make(Entity.ENTITY_SKELETON2,450,390,50,50))

        # WALLS
        self.addbox(0,0,500,200)
        # NORTHEAST EXIT
        self.addexit(500,0,100,160,3003,310,230,Room2World(self.roomman))
        # WEST EXIT
        ###FIXself.addexit(0,200,60,280,3002,310,230,Room2World(self.roomman))

        # NORTH EXIT
#        self.addexit(0,0,640,80,3001,310,230,Roomforest1(0,-420, self.roomman))

        # DUNGEON 1 GATE
#        self.addexit(350,200,90,80,1.1,210,390,Room1dungeon1())

        # SOUTH EXIT
#        self.addexit(0,430,640,50,1002,210,90,Room2world(self.roomman))

        # EXITS
#        self.addexit(0,0,640,100,1,100,100)

        self.background = pygame.image.load('./pics/worldroom1.bmp').convert()


class Room2world(Maproom):
    ""
    def __init__(self, roomman):
        ###Maproom.__init__(self,-320,0)
        ### Maproom.__init__(self,-700,-700)##FIXME1
        Maproom.__init__(self,0,-300,font)##FIXME1
	self.roomman = roomman
	self.player = self.roomman.player
	###FIXaddonself.octtree = Octtree(500,500,500,300,300,300,1,self)
	self.octtree = Octtree(500,500,500,300,300,300,0,self)
	self.octtree.generatetree(100)
	self.trees.addobject(self.octtree)
	self.octtree = Octtree(1000,400,1000,300,300,300,1,self)
	self.octtree.generatetree(400)
	self.trees.addobject(self.octtree)

    def loadroom(self):
        self.initroom()
        self.loadworldroom2()
        return self


    def loadworldroom2(self):
       
        factory = Entityfactory()
        self.addfactory(factory)

#        self.addentity(factory.make(Entity.ENTITY_TREE2, 0,0,130,130))
#        self.addenemy(factory.make(Entity.ENTITY_MYCONID,400,390,50,50))     
        self.addenemy(factory.makewith(Entity.ENTITY_BOMBMAN1,450,90,333,333,self.boxes,self.entities,self.player))

        ###self.addblock(0,0,750,500,200,200)
        # WALLS
#        self.addbox(0,0,500,200)
        # NORTHEAST EXIT
#        self.addexit(580,0,100,400,3001,310,230,Room2World(0,0))
        # WEST EXIT
#        self.addexit(0,200,60,60,1.6,570,210)

        # NORTH EXIT
#        self.addexit(0,0,640,80,3001,310,230,Roomforest1(0,-420, self.roomman))

        # DUNGEON 1 GATE
#        self.addexit(350,200,90,80,1.1,210,390,Room1dungeon1())

        # SOUTH EXIT
#        self.addexit(0,430,640,50,1002,210,90,Room2world(self.roomman))

        # EXITS
#        self.addexit(0,0,640,100,1,100,100)

        self.background = pygame.image.load('./pics/bgrot1-1000x1000.bmp').convert()
        ###self.background = pygame.image.load('./pics/blank.bmp').convert()

