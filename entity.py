
# Copyright (C) Johan Ceuppens 2011
# Copyright (C) Johan Ceuppens 2010

# Copyright (C) Johan Ceuppens 2009 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import pygame
from pygame.locals import *

from random import *
import os
from utility import *

class Entity(object):


    ENTITY_CAPRICORNGUARDIAN3,ENTITY_BAT, ENTITY_SKELETON,ENTITY_SKELETON2, ENTITY_SKELETONUNDERPLAYERCOMMAND, ENTITY_TREE2,ENTITY_TREE3,ENTITY_TREE4,ENTITY_TREE5,ENTITY_ENT,ENTITY_MYCONID,ENTITY_MYCONID_RED,ENTITY_EARTHWEIRD,ENTITY_KOBOLDMAGE,ENTITY_ORCPALADIN,ENTITY_ANGEL,ENTITY_HOBGOBLINSWORDSMAN1,ENTITY_DEMON1,ENTITY_HOBGOBLINSWORDSMANCHIEF1,ENTITY_PLATFORM1,ENTITY_PLATFORMMAPROOM1,ENTITY_CHERUBSTATUE1,ENTITY_FOOD,ENTITY_BATCRAZY,ENTITY_BOUNCER1,ENTITY_KNIGHTDARK1,ENTITY_HOUNDARCHON1,ENTITY_OOZEBIG1,ENTITY_WATERDEVIL,ENTITY_WATERDEVILFEMALE,ENTITY_TREESTONE,ENTITY_ENSORCELLEDTREE,ENTITY_NUTTER1,ENTITY_SPARROW,ENTITY_NUTTER2,ENTITY_TROLLBLACK,ENTITY_UNICORNSTATUE,ENTITY_UNICORNSTATUELITTLE,ENTITY_HERMIT,ENTITY_CANDLEFLAME1,ENTITY_CANDLEFLAME2,ENTITY_ORCSWORDSMANBLUE,ENTITY_SHIELDMAIDEN,ENTITY_CHAOSKNIGHT1,ENTITY_CHAOSKNIGHT2,ENTITY_OOZE,ENTITY_BOMBMAN1 = xrange(47)

    def __init__(self,x,y,w,h):
        self.x = x
        self.y = y
        self.w = w
        self.h = h

        self.images = []
        self.imagescounter = 0
        self.maximages = 0
        
        self.talksystem = None

	#self.platform = 0

	self.hitpoints = 100#FIXME in entities like trees
#NOTE        self.name = "Entity"

    def doplatform(self,player,level):
	1

    def dofood(self,player):
	1

    def leftbuttonpressed(self,game):#,mouse,text,press0):
        print "+++ left button press on entity"
        return None

    def leftbuttonpressedwithinventoryitem(self,game):
        print "+++ left button press on entity with inventory item"
        if game.mouse.press(0):
            return 1
        return None


    def middlebuttonpressed(self,mouse):
        1

    def rightbuttonpressed(self,mouse):
        1


    def getname(self,text):
        text.getname(self)

    def getnametext(self,text):
        return "Entity"

    def addimage(self,filename,r,g,b):# rgb transparency values
        self.maximages += 1

        image = pygame.image.load(filename).convert()
        image.set_colorkey((r,g,b))
        self.images.append(image)
        
#    def auxdraw(self):
#	self.auxdraw(self)

    def draw(self,screen):
        self.imagescounter += 1
        if self.imagescounter == self.maximages:
            self.imagescounter = 0     
        screen.scr.blit(self.images[self.imagescounter], (self.x, self.y))

    def drawonmap(self, screen, maproom):
        self.imagescounter += 1
        if self.imagescounter == self.maximages:
            self.imagescounter = 0     
        screen.scr.blit(self.images[self.imagescounter], (self.x+maproom.relativex, self.y+maproom.relativey))

    
    def update(self,room):#FIXME all level arg must be room

        r = randint(0,3)
        if r == 0:
            self.x += 1
        if r == 1:
            self.x -= 1
        if r == 2:
            self.y += 1
        if r == 3:
            self.y -= 1

    def decreasehpinplayer(self, player, level):
	1	
    
    def hit(self):
        1

    def bounceback(self):
        1

    def remove(self,list):
        1

    def deathupdate(self):
        1

    def destroy(self):
        1

    def collisionlist(self,collider,list):
        for i in range(0,list.length):
            o = None
            o = list.getlistobject(o)
            if o and collider.x >= o.x and collider.x <= o.x + o.w and collider.y >= o.y and collider.y <= o.y + o.h:   	
                return o
        return None

    def addtolist(self,list):
        list.addobject(self)

    def isskeleton(self):
        return None

    def isplayerskeleton(self):
        return None

    # FIXME enemy
    def usedracolichspellbook(self,game):
        game.screen.scr.blit(game.font.render("Nothing happens!", 16, (255,255,255)), (10,10))
	return "Nothing happens!"	
        
    def useyellowring(self,game):
        game.screen.scr.blit(game.font.render("Nothing happens!", 16, (255,255,255)), (10,10))
	return "Nothing happens!"	
    
    def usescrollinvisibility(self,game):
        game.screen.scr.blit(game.font.render("Nothing happens!", 16, (255,255,255)), (10,10))
	return "Nothing happens!"	

    def setblackplayer(self,game):
        print "+++ set black entity"
        1


	# FOR mouse CLICKS WITH ITEMS
    def leftbuttonpressedwithinventoryitem(self,game):
        if game.mouse.press(0) == 1:
            self.talkcounter += 1
            if game.inventoryitem:
                game.inventoryitem.use(self,game)
            return 1
        else:
            self.talkcounter = 0 	
            return None#self.currenttext


