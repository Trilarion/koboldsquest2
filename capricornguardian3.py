
# Copyright (C) Johan Ceuppens 2011
# Copyright (C) Johan Ceuppens 2010

# Copyright (C) Johan Ceuppens 2009 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from enemytalking import *
from talksystemcapricornguardian3 import *
from entityfactory import *
from food import *

class Capricornguardian3(Enemytalking):
    def __init__(self,x,y,w,h, boxes, entities, player):
    ###def __init__(self,x,y,w,h):
        Enemytalking.__init__(self,x,y,w,h,Talksystemcapricornguardian3())
        self.addimage('./pics/capricornguardian1.bmp',0,0,0)
        self.addimage('./pics/capricornguardian1.bmp',0,0,0)
        self.addimage('./pics/capricornguardian1.bmp',0,0,0)
        self.addimage('./pics/capricornguardian1.bmp',0,0,0)
        self.addimage('./pics/capricornguardian2.bmp',0,0,0)
        self.addimage('./pics/capricornguardian2.bmp',0,0,0)
        self.addimage('./pics/capricornguardian2.bmp',0,0,0)
        self.addimage('./pics/capricornguardian2.bmp',0,0,0)
        self.addimage('./pics/capricornguardian3.bmp',0,0,0)
        self.addimage('./pics/capricornguardian3.bmp',0,0,0)
        self.addimage('./pics/capricornguardian3.bmp',0,0,0)
        self.addimage('./pics/capricornguardian3.bmp',0,0,0)
        self.addimage('./pics/capricornguardian2.bmp',0,0,0)
        self.addimage('./pics/capricornguardian2.bmp',0,0,0)
        self.addimage('./pics/capricornguardian2.bmp',0,0,0)
        self.addimage('./pics/capricornguardian2.bmp',0,0,0)

	self.images2 = []
        self.addimage2('./pics/capricornguardianup1.bmp',0,0,0)
        self.addimage2('./pics/capricornguardianup1.bmp',0,0,0)
        self.addimage2('./pics/capricornguardianup1.bmp',0,0,0)
        self.addimage2('./pics/capricornguardianup1.bmp',0,0,0)
        self.addimage2('./pics/capricornguardianup2.bmp',0,0,0)
        self.addimage2('./pics/capricornguardianup2.bmp',0,0,0)
        self.addimage2('./pics/capricornguardianup2.bmp',0,0,0)
        self.addimage2('./pics/capricornguardianup2.bmp',0,0,0)
        self.addimage2('./pics/capricornguardianup3.bmp',0,0,0)
        self.addimage2('./pics/capricornguardianup3.bmp',0,0,0)
        self.addimage2('./pics/capricornguardianup3.bmp',0,0,0)
        self.addimage2('./pics/capricornguardianup3.bmp',0,0,0)
        self.addimage2('./pics/capricornguardianup2.bmp',0,0,0)
        self.addimage2('./pics/capricornguardianup2.bmp',0,0,0)
        self.addimage2('./pics/capricornguardianup2.bmp',0,0,0)
        self.addimage2('./pics/capricornguardianup2.bmp',0,0,0)

        self.counter = 0
	self.hitpoints = 10
	self.maxhitpoints = 10
	self.direction = "down"
	self.boxes = boxes
	self.entities = entities 
	self.player = player

####### TEXT / FONT interface
    def getname(self,text):
        return text.getcapricornguardianname()

#### NEW FUNCTION
    def addimage2(self,filename,r,g,b):# rgb transparency values
### NOTE same number of images already        self.maximages2 += 1

        image2 = pygame.image.load(filename).convert()
        image2.set_colorkey((r,g,b))
        self.images2.append(image2)

    def collisionboxesnew(self,room):
        o = self.collisionlist(room.boxes)
        if o:
            #self.bounceback()#FIXME1 level arg
            return 1
        return None

    def update(self,room):
##	if self.entangled:
		return
###	if self.hitpoints < self.maxhitpoints:
###		room	
###		return

####### other methods
    def drawonmap(self, screen, maproom):
	if self.direction == "down": 
        	screen.scr.blit(self.images[0], (self.x+maproom.relativex, self.y+maproom.relativey))
	elif self.direction == "up": 
        	screen.scr.blit(self.images2[0], (self.x+maproom.relativex, self.y+maproom.relativey))

    def draw(self,screen):
        self.counter += 1

        self.imagescounter += 1
	if self.direction == "down": 
        	screen.scr.blit(self.images[0], (self.x, self.y))
	elif self.direction == "up": 
        	screen.scr.blit(self.images2[0], (self.x, self.y))
#	if self.entangled:
#        	rootsimage = pygame.image.load('entangleroots1.bmp').convert()
#        	rootsimage.set_colorkey((0,0,0))
#        	screen.scr.blit(rootsimage, (self.x, self.y+self.h-40))
        

#     def leftbuttonpressed(self,mouse,text):
#         return self.talksystem.gettalk(text)#foo

    def remove(self,list):
	### WITH PLAYER COLLISION ON o YOU RESHOOT
###	list.add(LightningProbeBullet(self.x,self.y,50,50,self.boxes,self.player))

        if self.hitpoints <= 0:
            list.remove(self)

	
    def leftbuttonpressedwithinventoryitem(self,game):
        if Enemytalking.leftbuttonpressedwithinventoryitem(self,game):
            print "You use the item on the knight."
            return 1
        else:
            return None



    def collisionboxes(self):
        o = self.collisionlist(self.boxes)
        
        if o:
            #player.bounceback()#FIXME1 level arg
            return 1
        return None

    def bounceback(self):
        if self.direction == "up":#north
	    self.y += 12 
	   # self.direction = 3 
        elif self.direction == "down":#south
	    self.y -= 12
	   # self.direction = 1 
        elif self.direction == "left":#west
	    self.x -= 12
	   # self.direction = 2 
        elif self.direction == "right":#east
	    self.x += 12
	   # self.direction = 4 
	# set direction
	#r = randint(1,4)
	#self.direction = r

    def collisionlist(self,list):
        return self.collisionlist1(self,list)

    # GEN PROC
    def collisionlist1(self,collider,list):
        for i in range(0,list.length):
            o = None
            o = list.getlistobject(o)
            
            if o:
#                print 'i=%d x=%d y=%d px=%d py=%d' % (i, o.x,o.y,collider.x,collider.y)
                if collider.x >= o.x and collider.x <= o.x + o.w and collider.y >= o.y and collider.y <= o.y + o.h :   	
                    return o
        return None

    def donotslay(self, player):
	if self.hitpoints < self.maxhitpoints:
		r = randint(0,1000)
		if r >= 995:
			self.entities.add(LightningProbeBullet(self.x,self.y,50,50,self.boxes,self.player))
		player.donotslay()

    def usegoldcoin(self,game):
        string = "You have used the gold coin"
        string += "\n"

	s = ""

        f = open("./workfile",'r')
        for line in f:
            s += line
                
        f = open("./workfile",'w')
        
        f.write(s)
        f.write(string)
        f.write("\n")

	self.talksystem = Talksystemangelspellbookdestroy()
	self.usedspellbookdracolich = 1
	coin = Goldcoin(300,400,80,80,"1","1")
	game.level.roommanager.room.entities.add(coin)
	### coin.remove(game.level.roommanager.room.entities)

	game.level.roommanager.room.entities.remove(self)
        return game.text.getcapricornguardiandisappear()

    def hitcommand(self, player,type, value, damagevalue):
        player.hitcommand(player, type, value, damagevalue)	
